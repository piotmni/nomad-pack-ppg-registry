app {
  url = "https://gitlab.com/piotmni/nomad-pack-ppg-registry/"
  author = "PPG"
}

pack {
  name = "ppg_handlers"
  description = "This pack contains template for creating handlers"
  url = "https://gitlab.com/piotmni/ppg-nomad-pack-registry/ppg_handlers"
  version = "0.0.1"
}
