[[ define "task" ]]
task "[[ .ppg_handlers.task_name ]]" {
    driver = "docker"
    config {
        image = "[[ .ppg_handlers.version ]]"

        [[- if .ppg_handlers.work_dir ]]
        work_dir = "[[ .ppg_handlers.work_dir ]]"
        [[- end ]]

        [[- if .ppg_handlers.command ]]
        command = "[[ .ppg_handlers.command ]]"
        [[- end ]]

        [[- if .ppg_handlers.args ]]
        args = [[ .ppg_handlers.args | toJson ]]
        [[- end ]]

        [[- if .ppg_handlers.extra_hosts ]]
        extra_hosts = [[ .ppg_handlers.extra_hosts | toJson ]]
        [[- end ]]

        [[- if .ppg_handlers.network_mode  ]]
        network_mode = "[[ .ppg_handlers.network_mode ]]"
        [[- end ]]

        auth {
          username = "[[ env "DOCKER_USER" ]]"
          password = "[[ env "DOCKER_PASSWORD" ]]"
          server_address = "[[ env "DOCKER_SERVER_ADDR" ]]"
        }

        [[- if .ppg_handlers.ports ]]
        ports = [[ .ppg_handlers.ports | toJson ]]
        [[- end ]]

        [[- if .ppg_handlers.labels ]]
        labels {
            [[- range $key, $val := .ppg_handlers.labels ]]
            [[ $key ]] = [[ $val ]]
            [[- end ]]
        }
        [[- end ]]

    }

     vault {
       policies = [
         "allow-nomad-client"
       ]
     }

     [[- if .ppg_handlers.envs ]]
     env {
        [[- range $key, $val := .ppg_handlers.envs ]]
        [[ $key ]] = [[ $val ]]
        [[- end ]]
     }
     [[- end ]]


     resources{
        memory        = [[ .ppg_handlers.resources.memory ]]
        cpu           = [[ .ppg_handlers.resources.cpu ]]
        memory_max    = [[ .ppg_handlers.resources.memory_max ]]
     }

     [[- if .ppg_handlers.vault_files ]]
     [[- range $vault_template := .ppg_handlers.vault_files ]]
     template {
        data = <<EOF
{{- with secret "[[ $vault_template.key ]]" }}
{{- base64Decode .Data.data.[[ $vault_template.value ]] }}
{{- end }}
EOF
        destination = "[[ $vault_template.destination ]]"
     }
     [[- end ]]
     [[- end ]]


     [[- if .ppg_handlers.templates ]]
     [[- range $template := .ppg_handlers.templates ]]
     template {
        data = <<EOF
[[ $template.data ]]
EOF

        destination = "[[ $template.destination ]]"
        [[- if $template.env ]]
        env = "[[ $template.env ]]"
        [[- end ]]

        [[- if $template.change_mode ]]
        change_mode = "[[ $template.change_mode ]]"
        [[- end ]]

     }
     [[- end ]]
     [[- end ]]

}
[[ end ]]
