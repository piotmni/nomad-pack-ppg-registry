job "[[ .ppg_handlers.job_name ]]" {

  type = "service"

  region      = "[[.ppg_handlers.region]]"
  datacenters =  [[.ppg_handlers.datacenters | toJson ]]
  namespace   = "[[.ppg_handlers.namespace]]"

  spread {
    attribute = "${attr.unique.hostname}"
  }

  update {
    canary            = "[[ .ppg_handlers.canary ]]"
    max_parallel      = "[[ .ppg_handlers.max_parallel ]]"
    progress_deadline = "[[ .ppg_handlers.progress_deadline ]]"
    min_healthy_time  = "[[ .ppg_handlers.min_healthy_time ]]"
    auto_revert       = [[ .ppg_handlers.auto_revert ]]
    auto_promote      = [[ .ppg_handlers.auto_promote ]]
  }

  constraint {
    attribute = "${meta.destiny}"
    value = "worker"
  }

  group "group-[[ .ppg_handlers.task_name ]]" {
      count = [[ .ppg_handlers.count ]]


      [[ template "task" . ]]

      restart {
        interval = "1m"
        attempts = 5
        delay    = "5s"
        mode = "fail"
      }

      // it is mainly static for handlers
      service {
        name = "[[ .ppg_handlers.task_name ]]"
        [[ if .ppg_handlers.tags ]
        tags = [[ .ppg_handlers.tags | toJson ]]
        [[ end ]]
        meta {
          version = "[[ .ppg_handlers.version ]]"
        }
        canary_meta {
          canary = "true"
          version = "[[ .ppg_handlers.version ]]"
        }

        check {
          type = "http"
          port = "healthcheck"
          path = "/healthz"
          interval = "10s"
          timeout = "5s"
          check_restart {
              grace = "15s"
              limit = "3"
          }
        }
      }
      network {
        port "healthcheck" {
          to = 8001
        }
      }
   }
}
