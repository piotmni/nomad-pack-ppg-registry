app {
  url = "https://gitlab.com/piotmni/nomad-pack-ppg-registry/"
  author = "PPG"
}

pack {
  name = "ppg_server"
  description = "This pack contains template for creating servers"
  url = "https://gitlab.com/piotmni/ppg-nomad-pack-registry/ppg_server"
  version = "0.0.1"
}
