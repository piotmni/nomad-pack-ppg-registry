job "[[ .ppg_server.job_name ]]" {

  type = "service"

  region      = "[[.ppg_server.region]]"
  datacenters =  [[.ppg_server.datacenters | toJson ]]
  namespace   = "[[.ppg_server.namespace]]"

  spread {
    attribute = "${attr.unique.hostname}"
  }

  update {
    canary            = "[[ .ppg_server.canary ]]"
    max_parallel      = "[[ .ppg_server.max_parallel ]]"
    progress_deadline = "[[ .ppg_server.progress_deadline ]]"
    min_healthy_time  = "[[ .ppg_server.min_healthy_time ]]"
    auto_revert       = [[ .ppg_server.auto_revert ]]
    auto_promote      = [[ .ppg_server.auto_promote ]]
  }

  constraint {
    attribute = "${meta.destiny}"
    value = "worker"
  }

  group "group-[[ .ppg_server.task_name ]]" {
    count = [[ .ppg_server.count ]]

    [[ template "task" . ]]

    service {
      name = "[[ .ppg_server.task_name ]]"
      [[ if .ppg_server.tags ]]
      tags = [[  .ppg_server.tags | toJson ]]
      [[ end ]]
      meta {
        version = "[[ .ppg_server.version ]]"
      }
      canary_meta {
        canary = "true"
        version = "[[ .ppg_server.version ]]"
      }

      check {
        type = "http"
        port = "healthcheck"
        path = "/healthz"
        interval = "10s"
        timeout = "5s"
        check_restart {
            grace = "15s"
            limit = "3"
        }
      }
    }

    restart {
      interval = "1m"
      attempts = 5
      delay    = "5s"
      mode = "fail"
    }

    service {
      name = "[[ .ppg_server.backend_service_name ]]"
      tags = [[ .ppg_server.tags_backend | toJson ]]
      port = "backend"
      check {
        type = "http"
        port = "backend"
        path = "/health"
        interval = "10s"
        timeout = "5s"
        check_restart {
            grace = "15s"
            limit = "3"
        }
      }
    }

    network {
      port "healthcheck" {
        to = 8001
      }
      port "backend" {
        to = 3000
      }
    }
  }
}
