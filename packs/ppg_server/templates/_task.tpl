[[ define "task" ]]
task "[[ .ppg_server.task_name ]]" {
    driver = "docker"
    config {
        image = "[[ .ppg_server.version ]]"

        [[- if .ppg_server.work_dir ]]
        work_dir = "[[ .ppg_server.work_dir ]]"
        [[- end ]]

        [[- if .ppg_server.command ]]
        command = "[[ .ppg_server.command ]]"
        [[- end ]]

        [[- if .ppg_server.args ]]
        args = [[ .ppg_server.args | toJson ]]
        [[- end ]]

        [[- if .ppg_server.extra_hosts ]]
        extra_hosts = [[ .ppg_server.extra_hosts | toJson ]]
        [[- end ]]

        [[- if .ppg_server.network_mode  ]]
        network_mode = "[[ .ppg_server.network_mode ]]"
        [[- end ]]

        auth {
          username = "[[ env "DOCKER_USER" ]]"
          password = "[[ env "DOCKER_PASSWORD" ]]"
          server_address = "[[ env "DOCKER_SERVER_ADDR" ]]"
        }

        [[- if .ppg_server.ports ]]
        ports = [[ .ppg_server.ports | toJson ]]
        [[ -end ]]

        [[- if .ppg_server.labels ]]
        labels {
            [[- range $key, $val := .ppg_server.labels ]]
            [[ $key ]] = [[ $val ]]
            [[- end ]]
        }
        [[- end ]]

    }

     vault {
       policies = [
         "allow-nomad-client"
       ]
     }

     [[- if .ppg_server.envs ]]
     env {
        [[- range $key, $var := .ppg_server.envs ]]
        [[ $key ]] = [[ $val ]]
        [[- end ]]
     }
     [[- end ]]


     resources{
        memory        = [[ .ppg_server.resources.memory ]]
        cpu           = [[ .ppg_server.resources.cpu ]]
        memory_max    = [[ .ppg_server.resources.memory_max ]]
     }

     [[- if .ppg_server.vault_files ]]
     [[- range $vault_template := .ppg_server.vault_files ]]
     template {
        data = <<EOF
{{- with secret "[[ $vault_template.key ]]" }}
{{- base64Decode .Data.data.[[ $vault_template.value ]] }}
{{- end }}
EOF
        destination = "[[ $vault_template.destination ]]"
     }
     [[- end ]]
     [[- end ]]


     [[- if .ppg_server.templates ]]
     [[- range $template := .ppg_server.templates ]]
     template {
        data = <<EOF
[[ $template.data ]]
EOF

        destination = "[[ $template.destination ]]"
        [[- if $template.env ]]
        env = "[[ $template.env ]]"
        [[- end ]]

        [[- if $template.change_mode ]]
        change_mode = "[[ $template.change_mode ]]"
        [[- end ]]

     }
     [[- end ]]
     [[- end ]]

}
[[ end ]]
