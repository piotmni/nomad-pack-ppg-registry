app {
  url = "https://gitlab.com/piotmni/nomad-pack-ppg-registry/"
  author = "PPG"
}

pack {
  name = "ppg_ues_cases"
  description = "This pack contains template for creating use_cases"
  url = "https://gitlab.com/piotmni/ppg-nomad-pack-registry/ppg_use_cases"
  version = "0.0.1"
}
