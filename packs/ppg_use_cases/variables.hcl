variable "job_name" {
  description = "The name to use as the job name which overrides using the pack name."
  type        = string
  default     = ""
}

variable "datacenters" {
  description = "A list of datacenters in the region which are eligible for job placement."
  type        = list(string)
  default     = ["dc1"]
}

variable "region" {
  description = "The region where the job should be placed."
  type        = string
  default     = "global"
}

variable "namespace" {
  description = "The namespace where the job should be placed."
  type        = string
  default     = "default"
}


variable "count" {
  description = "Count of running tasks"
  type        = number
  default     = 1
}

variable "task_name" {
  description = "Task name"
  type        = string
}

variable "network" {
  description = "Definition of ports"
  type        = list(object({
    name = string
    port = number
  }))
}

variable "command" {
  description = "Definition of commands"
  type        = string
}

variable "work_dir" {
  description = "Definition of workdir"
  type        = string
}

variable "args" {
  description = "Definition of args"
  type        = list(string)
}

variable "version" {
  description = "Definition of task version"
  type        = string
}

variable "ports"{
  description = "Definition of ports"
  type        = list(string)
}


variable "extra_hosts"{
  description = "Definition of extra_hosts"
  type        = list(string)
}

variable "labels" {
  description = "Definition of labels"
  type        = map(string)
}

variable "envs" {
  description = "Definition of envs"
  type        = map(string)
}

variable "vault_files" {
  description = "definition of files whose content is a variable in a vault"
  type        = list(object({
    path          = string
    destination   = string
    key           = string
  }))
}

variable "templates" {
  description = "definition of templates "
  type        = list(object({
    data          = string
    destination   = string
    env           = string
    change_mode   = string
  }))
}

variable "resources" {
  description = "Definition of resources"
  type        = object({
    memory          = number
    memory_max      = number
    cpu             = number
  })
}

variable "cron" {
  description = "Definition of how often use_cases should be run."
  type        = string
}

variable "tags" {
  description = "A list of tags attached to services"
  type        = list(string)
}