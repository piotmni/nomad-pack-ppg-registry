job "[[ .ppg_use_cases.job_name ]]" {

  type = "batch"

  region      = "[[.ppg_use_cases.region]]"
  datacenters =  [[.ppg_use_cases.datacenters | toJson ]]
  namespace   = "[[.ppg_use_cases.namespace]]"

  spread {
    attribute = "${attr.unique.hostname}"
  }

  periodic {
    cron = "[[ .ppg_use_cases.cron ]]"
    prohibit_overlap = true
    time_zone = "Europe/Warsaw"
  }

  constraint {
    attribute = "${meta.destiny}"
    value = "worker"
  }

  group "group-[[ .ppg_use_cases.task_name ]]" {
    count = [[ .ppg_use_cases.count ]]

    [[ template "task" . ]]

    restart {
      attempts = 0
      mode = "fail"
    }

    service {
      name = "[[ .ppg_use_cases.task_name ]]"
      [[ .ppg_use_cases.tags  ]]
      tags = [[  .ppg_use_cases.tags | toJson ]]
      [[ end ]]
      meta {
        version = "[[ .ppg_use_cases.version ]]"
      }
      canary_meta {
        canary = "true"
        version = "[[ .ppg_use_cases.version ]]"
      }
    }
  }
}
