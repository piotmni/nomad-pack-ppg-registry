[[ define "task" ]]
task "[[ .ppg_use_cases.task_name ]]" {
    driver = "docker"
    config {
        image = "[[ .ppg_use_cases.version ]]"

        [[- if .ppg_use_cases.work_dir ]]
        work_dir = "[[ .ppg_use_cases.work_dir ]]"
        [[- end ]]

        [[- if .ppg_use_cases.command ]]
        command = "[[ .ppg_use_cases.command ]]"
        [[- end ]]

        [[- if .ppg_use_cases.args ]]
        args = [[ .ppg_use_cases.args | toJson ]]
        [[- end ]]

        [[- if .ppg_use_cases.extra_hosts ]]
        extra_hosts = [[ .ppg_use_cases.extra_hosts | toJson ]]
        [[- end ]]

        [[- if .ppg_use_cases.network_mode  ]]
        network_mode = "[[ .ppg_use_cases.network_mode ]]"
        [[- end ]]

        auth {
          username = "[[ env "DOCKER_USER" ]]"
          password = "[[ env "DOCKER_PASSWORD" ]]"
          server_address = "[[ env "DOCKER_SERVER_ADDR" ]]"
        }

        [[- if .ppg_use_cases.ports ]]
        ports = [[ .ppg_use_cases.ports | toJson ]]
        [[- end ]]

        [[- if .ppg_use_cases.labels ]]
        labels {
            [[- range $key, $var := .ppg_use_cases.labels ]]
            [[ $key ]] = [[ $val ]]
            [[- end ]]
        }
        [[- end ]]

    }

     vault {
       policies = [
         "allow-nomad-client"
       ]
     }

     [[- if .ppg_use_cases.envs ]]
     env {
        [[- range $key, $val := .ppg_use_cases.envs ]]
        [[ $key ]] = [[ $val ]]
        [[- end ]]
     }
     [[- end ]]


     resources{
        memory        = [[ .ppg_use_cases.resources.memory ]]
        cpu           = [[ .ppg_use_cases.resources.cpu ]]
        memory_max    = [[ .ppg_use_cases.resources.memory_max ]]
     }

     [[- if .ppg_use_cases.vault_files ]]
     [[- range $vault_template := .ppg_use_cases.vault_files ]]
     template {
        data = <<EOF
{{- with secret "[[ $vault_template.key ]]" }}
{{- base64Decode .Data.data.[[ $vault_template.value ]] }}
{{- end }}
EOF
        destination = "[[ $vault_template.destination ]]"
     }
     [[- end ]]
     [[- end ]]


     [[- if .ppg_use_cases.templates ]]
     [[- range $template := .ppg_use_cases.templates ]]
     template {
        data = <<EOF
[[ $template.data ]]
EOF

        destination = "[[ $template.destination ]]"
        [[- if $template.env ]]
        env = "[[ $template.env ]]"
        [[- end ]]

        [[- if $template.change_mode ]]
        change_mode = "[[ $template.change_mode ]]"
        [[- end ]]

     }
     [[- end ]]
     [[- end ]]

}
[[ end ]]
